## Home

### Basics

In these complicated times, you should do yourself a favor and simplify yours and your collaborators' lives by learning a few tricks, and some useful tools that any computer scientist and researcher should know. A very good start is the MIT course [The Missing Semester of Your CS Education](https://missing.csail.mit.edu/). I strongly suggest taking a look at the entire course.

### Command line

- Start from here:
  - [MIT course - Command Line environments, dotfiles, tmux](https://missing.csail.mit.edu/2020/command-line/)

### Git

- Git is the first and most important tool you need.
- [MIT course - Version Control (Git) to master](https://missing.csail.mit.edu/2020/version-control/)



## IDEs

### VS Code

- [Visual Studio Code - Code Editing. Redefined](https://code.visualstudio.com/)
- [Remote Extension to access servers](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack)
- [10 Visual Studio Code extensions for Python development](https://medium.com/issuehunt/10-visual-studio-code-extensions-for-python-development-de0be51bbeed)
- [Linting Python in Visual Studio Code](https://code.visualstudio.com/docs/python/linting)

### PyCharm

- [PyCharm: the Python IDE for Professional Developers by JetBrains](https://www.jetbrains.com/pycharm/)

### Vim

- [MIT course - Editors (Vim)](https://missing.csail.mit.edu/2020/editors/)



## Writing and Research

### LaTeX

Highly recommended [Overleaf](https://www.overleaf.com/) as an online collaborative LaTeX environment.

### Zotero

Zotero is a management tool to track and organize papers. Unfortunately, it has limited online storage, so it is strongly recommended to save files using the `zotfile` extension that allows you to decouple actual files from the library using symbolic links. You can use your dropbox or gdrive space, to organize papers in your file system replicating the structure of your Zotero library. This is particularly useful if you want to read and take notes on a tablet and synchronize the changes on all of your devices.

- [Zotero | Your personal research assistant](https://www.zotero.org/)
- [ZotFile - Advanced PDF management for Zotero](http://zotfile.com/)
- [Using Zotfile for Tablet Syncing and More - Zotero @ WSU Libraries](https://libguides.libraries.wsu.edu/c.php?g=768677&p=5514204)
- [(PDF) Tutorial: The Best Reference Manager Setup (Zotero + ZotFile + Cloud Storage)](https://www.researchgate.net/publication/325828616_Tutorial_The_Best_Reference_Manager_Setup_Zotero_ZotFile_Cloud_Storage)

### Research on ML

- [An Opinionated Guide to ML Research](http://joschu.net/blog/opinionated-guide-ml-research.html)

### Deep learning

- [d2l.ai](http://d2l.ai/)

### General

- [The Missing Semester of Your CS Education](https://missing.csail.mit.edu/)
- [Awesome Course](https://gist.github.com/nurimelih/83f2055eb9ed506963797e13321fb9e1)
